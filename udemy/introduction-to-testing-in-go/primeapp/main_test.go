package main

import (
	"io"
	"os"
	"strings"
	"testing"
)

func Test_isPrime(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name     string
		args     args
		expected bool
		msg      string
	}{
		{
			name: "0 is not a prime",
			args: args{
				n: 0,
			},
			expected: false,
			msg:      "0 is not a prime, by definition!",
		},
		{
			name: "1 is not a prime",
			args: args{
				n: 1,
			},
			expected: false,
			msg:      "1 is not a prime, by definition!",
		},
		{
			name: "-4 is not a prime",
			args: args{
				n: -4,
			},
			expected: false,
			msg:      "Negative numbers are not a prime, by definition!",
		},
		{
			name: "15 is not a prime",
			args: args{
				n: 15,
			},
			expected: false,
			msg:      "15 is not a prime number because it is divisible by 3",
		},
		{
			name: "13 is a prime",
			args: args{
				n: 13,
			},
			expected: true,
			msg:      "13 is a prime number!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, msg := isPrime(tt.args.n)
			if result != tt.expected {
				t.Errorf("isPrime(%d) got = %v, want %v", tt.args.n, result, tt.expected)
			}
			if msg != tt.msg {
				t.Errorf("isPrime(%d) got1 = %v, want %v", tt.args.n, msg, tt.msg)
			}
		})
	}
}

func Test_prompt(t *testing.T) {
	// save a copy of os.Stdout
	oldOut := os.Stdout

	// create a pipe so we can read the output
	r, w, _ := os.Pipe()

	// set os.Stdout to our write pipe
	os.Stdout = w

	prompt()

	// close our writer
	_ = w.Close()

	// reset os.Stdout to what it was before
	os.Stdout = oldOut

	// read the output of our prompt function from our read pipe
	out, _ := io.ReadAll(r)

	// perform our test
	if string(out) != "-> " {
		t.Errorf("prompt() got = %v, want %v", string(out), "-> ")
	}
}

func Test_intro(t *testing.T) {
	// save a copy of os.Stdout
	oldOut := os.Stdout

	// create a pipe so we can read the output
	r, w, _ := os.Pipe()

	// set os.Stdout to our write pipe
	os.Stdout = w

	intro()

	// close our writer
	_ = w.Close()

	// reset os.Stdout to what it was before
	os.Stdout = oldOut

	// read the output of our prompt function from our read pipe
	out, _ := io.ReadAll(r)

	// perform our test
	if !strings.Contains(string(out), "Enter a whole number") {
		t.Errorf("intro() text not correct; got %s", string(out))
	}
}
